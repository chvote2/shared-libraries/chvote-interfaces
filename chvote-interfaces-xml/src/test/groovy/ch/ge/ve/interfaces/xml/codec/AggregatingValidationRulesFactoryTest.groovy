/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec

import ch.ge.ve.interfaces.xml.validation.rule.ValidationRule
import ch.ge.ve.interfaces.xml.validation.rule.ValidationRulesFactory
import spock.lang.Specification

class AggregatingValidationRulesFactoryTest extends Specification {

  def rule11 = Mock(ValidationRule)
  def rule12 = Mock(ValidationRule)
  def rule21 = Mock(ValidationRule)
  def rule22 = Mock(ValidationRule)
  def factory1 = Mock(ValidationRulesFactory)
  def factory2 = Mock(ValidationRulesFactory)
  def factory = new AggregatingValidationRulesFactory([factory1, factory2])

  def "constructor must refuse null inputs"() {
    when:
    code.call()

    then:
    thrown(NullPointerException)

    where:
    code << [
        { _ -> new AggregatingValidationRulesFactory(null) },
        { _ -> new AggregatingValidationRulesFactory([null]) },
        { _ -> new AggregatingValidationRulesFactory([null, Mock(ValidationRulesFactory)]) },
    ]
  }

  def "it should aggregate ech110 validation rules from multiple factories"() {
    when:
    def rules = factory.createEch110ValidationRules()

    then:
    1 * factory1.createEch110ValidationRules() >> [rule11, rule12]
    1 * factory2.createEch110ValidationRules() >> [rule21, rule22]
    rules == [rule11, rule12, rule21, rule22]
  }

  def "it should aggregate ech157 validation rules from multiple factories"() {
    when:
    def rules = factory.createEch157ValidationRules()

    then:
    1 * factory1.createEch157ValidationRules() >> []
    1 * factory2.createEch157ValidationRules() >> [rule21]
    rules == [rule21]
  }

  def "it should aggregate ech159 validation rules from multiple factories"() {
    when:
    def rules = factory.createEch159ValidationRules()

    then:
    1 * factory1.createEch159ValidationRules() >> [rule11]
    1 * factory2.createEch159ValidationRules() >> []
    rules == [rule11]
  }

  def "it should aggregate logistic validation rules from multiple factories"() {
    when:
    def rules = factory.createLogisticValidationRules()

    then:
    1 * factory1.createLogisticValidationRules() >> []
    1 * factory2.createLogisticValidationRules() >> []
    rules == []
  }

  def "it should aggregate counting circle list validation rules from multiple factories"() {
    when:
    def rules = factory.createCountingCircleListValidationRules()

    then:
    1 * factory1.createCountingCircleListValidationRules() >> [rule11]
    1 * factory2.createCountingCircleListValidationRules() >> [rule22]
    rules == [rule11, rule22]
  }
}
