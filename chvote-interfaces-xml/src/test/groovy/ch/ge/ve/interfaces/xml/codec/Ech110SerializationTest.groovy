/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec

import ch.ge.ve.interfaces.xml.ech.eCH0110.v4.Delivery
import ch.ge.ve.interfaces.xml.validation.BusinessValidationException
import ch.ge.ve.interfaces.xml.validation.Violation
import ch.ge.ve.interfaces.xml.validation.rule.ValidationRule
import ch.ge.ve.interfaces.xml.validation.rule.ValidationRulesFactory
import java.nio.charset.Charset
import org.junit.Assert
import org.xmlunit.builder.Input
import spock.lang.Specification

class Ech110SerializationTest extends Specification {

  private static final String SAMPLE_FILE_PATH = "/sample-eCH-0110-v4.xml"

  def "a valid eCH-0110 file should be successfully parsed"() {
    given: 'a valid eCH-0110 XML file'
    def stream = getClass().getResourceAsStream(SAMPLE_FILE_PATH)
    def codec = new XmlCodecFactory().ech110Codec()

    when:
    def delivery = codec.deserialize(stream)

    then: 'the file should be correctly parsed'
    delivery != null
    delivery.deliveryHeader.messageId == "5c0764e5ef9946658b3e10546a51885a"
    delivery.resultDelivery.contestInformation.contestIdentification == "201805 - VP du 06 mai 2018"
    delivery.resultDelivery.countingCircleResults.size() == 1
    delivery.resultDelivery.countingCircleResults.voteResults.size() == 1
  }

  def "a valid aggregated results delivery should be successfuly serialized to xml"() {
    given: 'a delivery generated from a valid eCH-0110 file'
    def file = SAMPLE_FILE_PATH
    def codec = new XmlCodecFactory().ech110Codec()
    def delivery = codec.deserialize(getClass().getResourceAsStream(file))

    when: 'serializing the delivery read from the eCH-0110 file'
    def out = new ByteArrayOutputStream()
    codec.serialize(delivery, out)

    then: 'serialization output should be similar to the original file'
    Assert.assertThat(Input.from(new String(out.toByteArray(), Charset.forName("UTF-8"))),
                      XmlMatchers.isSimilarTo(Input.fromStream(getClass().getResourceAsStream(file))))
  }

  def "it should apply business validation rules during deserialization when validation is enabled"() {
    given:
    def stream = getClass().getResourceAsStream(SAMPLE_FILE_PATH)
    def rules = [Mock(ValidationRule), Mock(ValidationRule)]
    def rulesFactory = Mock(ValidationRulesFactory)
    def codec = new XmlCodecFactory(rulesFactory).ech110Codec()

    when:
    codec.deserialize(stream)

    then:
    1 * rulesFactory.createEch110ValidationRules() >> rules
    1 * rules[0].validate(_) >> []
    1 * rules[1].validate(_) >> []
  }

  def "it should throw a business validation exception if some rules report violations during deserialization"() {
    given:
    def stream = getClass().getResourceAsStream(SAMPLE_FILE_PATH)
    def rules = [Mock(ValidationRule), Mock(ValidationRule), Mock(ValidationRule)]
    rules[0].validate(_) >> [new Violation("rule1", "error1"), new Violation("rule1", "error2")]
    rules[1].validate(_) >> []
    rules[2].validate(_) >> [new Violation("rule3", "error1")]
    def rulesFactory = Mock(ValidationRulesFactory)
    rulesFactory.createEch110ValidationRules() >> rules
    def codec = new XmlCodecFactory(rulesFactory).ech110Codec()

    when:
    codec.deserialize(stream)

    then:
    def exception = thrown(BusinessValidationException)
    exception.violations == [
        new Violation("rule1", "error1"), new Violation("rule1", "error2"), new Violation("rule3", "error1")
    ]
  }

  def "it should ignore disabled validation rules during deserialization"() {
    given:
    def stream = getClass().getResourceAsStream(SAMPLE_FILE_PATH)
    def rules = [Mock(ValidationRule), Mock(ValidationRule)]
    rules[0].getName() >> "rule1"
    rules[0].validate(_) >> [new Violation("rule1", "error1")]
    rules[1].validate(_) >> []
    def rulesFactory = Mock(ValidationRulesFactory)
    rulesFactory.createEch110ValidationRules() >> rules
    def configuration = new CodecConfiguration().withDisabledValidationRules("rule1")
    def codec = new XmlCodecFactory(rulesFactory).ech110Codec(configuration)

    when:
    codec.deserialize(stream)

    then:
    noExceptionThrown()
  }

  def "it should not apply business validation rules during deserialization when validation is disabled"() {
    given:
    def stream = getClass().getResourceAsStream(SAMPLE_FILE_PATH)
    def rulesFactory = Mock(ValidationRulesFactory)
    def codec = new XmlCodecFactory(rulesFactory).ech110Codec(new CodecConfiguration().withValidationDisabled())

    when:
    codec.deserialize(stream)

    then:
    0 * rulesFactory.createEch110ValidationRules() >> []
  }

  def "it should apply business validation rules during serialization when validation is enabled"() {
    given:
    def delivery = getValidDelivery()
    def rules = [Mock(ValidationRule), Mock(ValidationRule)]
    def rulesFactory = Mock(ValidationRulesFactory)
    def codec = new XmlCodecFactory(rulesFactory).ech110Codec()

    when:
    codec.serialize(delivery, new ByteArrayOutputStream())

    then:
    1 * rulesFactory.createEch110ValidationRules() >> rules
    1 * rules[0].validate(delivery) >> []
    1 * rules[1].validate(delivery) >> []
  }

  def "it should throw a business validation exception if some rules report violations during serialization"() {
    given:
    def delivery = getValidDelivery()
    def rules = [Mock(ValidationRule), Mock(ValidationRule), Mock(ValidationRule)]
    rules[0].validate(_) >> [new Violation("rule1", "error1"), new Violation("rule1", "error2")]
    rules[1].validate(_) >> []
    rules[2].validate(_) >> [new Violation("rule3", "error1")]
    def rulesFactory = Mock(ValidationRulesFactory)
    rulesFactory.createEch110ValidationRules() >> rules
    def codec = new XmlCodecFactory(rulesFactory).ech110Codec()

    when:
    codec.serialize(delivery, new ByteArrayOutputStream())

    then:
    def exception = thrown(BusinessValidationException)
    exception.violations == [
        new Violation("rule1", "error1"), new Violation("rule1", "error2"), new Violation("rule3", "error1")
    ]
  }

  def "it should ignore disabled validation rules during serialization"() {
    given:
    def delivery = getValidDelivery()
    def rules = [Mock(ValidationRule), Mock(ValidationRule)]
    rules[0].getName() >> "rule1"
    rules[0].validate(_) >> [new Violation("rule1", "error1")]
    rules[1].validate(_) >> []
    def rulesFactory = Mock(ValidationRulesFactory)
    rulesFactory.createEch110ValidationRules() >> rules
    def configuration = new CodecConfiguration().withDisabledValidationRules("rule1")
    def codec = new XmlCodecFactory(rulesFactory).ech110Codec(configuration)

    when:
    codec.serialize(delivery, new ByteArrayOutputStream())

    then:
    noExceptionThrown()
  }

  def "it should not apply business validation rules during serialization when validation is disabled"() {
    given:
    def delivery = getValidDelivery()
    def rulesFactory = Mock(ValidationRulesFactory)
    def codec = new XmlCodecFactory(rulesFactory).ech110Codec(new CodecConfiguration().withValidationDisabled())

    when:
    codec.serialize(delivery, new ByteArrayOutputStream())

    then:
    0 * rulesFactory.createEch110ValidationRules()
  }

  private Delivery getValidDelivery() {
    return new XmlCodecFactory().ech110Codec().deserialize(getClass().getResourceAsStream(SAMPLE_FILE_PATH))
  }
}
