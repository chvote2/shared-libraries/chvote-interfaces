/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.util

import static LocalDateTimeTestUtilities.localDate

import java.lang.reflect.Constructor
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Modifier
import spock.lang.Specification

/**
 * Unit test for JAXB date adapter.
 */
class LocalDateAdapterTest extends Specification {

  def "unmarshall method should parse an ISO date string to a LocalDateTime object"() {
    given:
    def date = '2017-07-25'

    when:
    def localDate = LocalDateAdapter.unmarshal(date)

    then:
    localDate == LocalDateTimeTestUtilities.localDate('25.07.2017')
  }

  def "unmarshall method should return null if unable to parse the date"() {
    given:
    def date = 'dummy date'

    when:
    def localDate = LocalDateAdapter.unmarshal(date)

    then:
    localDate == null
  }

  def "marshall method should marshall a LocalDateTime object to an ISO date string"() {
    given:
    def localDate = localDate('25.07.2017')

    when:
    def date = LocalDateAdapter.marshal(localDate)

    then:
    date == '2017-07-25'
  }

  def "constructor should be private"() {
    given:
    Constructor<LocalDateAdapter> constructor = LocalDateAdapter.class.getDeclaredConstructor()

    when:
    constructor.setAccessible(true)
    constructor.newInstance()

    then:
    Modifier.isPrivate(constructor.getModifiers())
    def e = thrown(InvocationTargetException)
    e.cause.getClass() == AssertionError
  }
}

