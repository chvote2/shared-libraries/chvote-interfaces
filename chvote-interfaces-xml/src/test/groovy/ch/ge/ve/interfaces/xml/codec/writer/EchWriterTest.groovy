/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.writer

import ch.ge.ve.interfaces.xml.ech.eCH0222.v1.Delivery
import javax.xml.bind.JAXBContext
import javax.xml.stream.XMLEventFactory
import javax.xml.stream.XMLOutputFactory
import spock.lang.Shared
import spock.lang.Specification

class EchWriterTest extends Specification {

  def namespace = "http://www.ge.ch/test"
  def encoding = "UTF-8"

  @Shared jaxbContext = JAXBContext.newInstance(Delivery)
  @Shared xmlEventFactory = XMLEventFactory.newInstance()
  @Shared xmlOutputFactory = XMLOutputFactory.newInstance()

  def "XML document is not validated by default"() {
    given:
    def out = new ByteArrayOutputStream(256)
    def eventWriter = xmlOutputFactory.createXMLEventWriter(out, encoding)
    def echWriter = new EchWriter(jaxbContext.createMarshaller(), xmlEventFactory, eventWriter, ["":namespace])

    when: 'we write custom XML, not conforming to the XSD'
    echWriter.openRootElement(namespace, "project")
    echWriter.writeStartElement(namespace, "contributor")
    echWriter.writeElement(namespace, "name", "Anonymous Developer")
    echWriter.writeEndElement(namespace, "contributor")
    echWriter.endRootElement(namespace, "project")

    then:
    noExceptionThrown()

    and:
    out.toString(encoding) == '<?xml version="1.0" encoding="UTF-8"?><project xmlns="http://www.ge.ch/test"><contributor><name>Anonymous Developer</name></contributor></project>'
  }

  def "element content is escaped"() {
    given:
    def out = new ByteArrayOutputStream(256)
    def eventWriter = xmlOutputFactory.createXMLEventWriter(out, encoding)
    def echWriter = new EchWriter(jaxbContext.createMarshaller(), xmlEventFactory, eventWriter, ["ns":namespace])

    when:
    echWriter.openRootElement(namespace, "project")
    echWriter.writeElement(namespace, "contributor", "John <b>Doe</b>")
    echWriter.endRootElement(namespace, "project")

    then:
    def result = out.toString(encoding)
    ! result.contains("John <b>Doe</b>")
    result.concat('<ns:contributor>John &lt;b&gt;Doe&lt;/b&gt;</ns:contributor>')
  }
}
