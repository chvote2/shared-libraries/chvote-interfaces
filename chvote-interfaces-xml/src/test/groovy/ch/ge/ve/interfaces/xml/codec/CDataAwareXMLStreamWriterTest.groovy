/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec

import javax.xml.stream.XMLStreamWriter
import spock.lang.Specification

class CDataAwareXMLStreamWriterTest extends Specification {

  private XMLStreamWriter delegate = Mock(XMLStreamWriter)
  private CDataAwareXMLStreamWriter writer = new CDataAwareXMLStreamWriter(delegate, "UTF-8")

  def "writeCharacters should use CDATA section if the text contains one of '&, <, >'"(String textContent) {
    when:
    writer.writeCharacters(textContent)

    then:
    1 * delegate.writeCData(textContent)
    0 * delegate.writeCharacters(_)

    where:
    textContent << [ "a&b", "1 < 2", "3 > 2" ]
  }

  def "writeCharacters should NOT use CDATA section if the text contains none of '&, <, >'"() {
    given:
    def textContent = "a text without special characters"

    when:
    writer.writeCharacters(textContent)

    then:
    0 * delegate.writeCData(_)
    1 * delegate.writeCharacters(textContent)
  }

  def "writeCharacters (range) should use CDATA section if the text portion contains one of '&, <, >'"(String textContent, int start, int len) {
    when:
    writer.writeCharacters(textContent.toCharArray(), start, len)

    then:
    1 * delegate.writeCData(textContent.substring(start, start + len))
    0 * delegate.writeCharacters(_)

    where:
    textContent         | start | len
    "one < two < three" | 3     | 6
    "one & two & three" | 3     | 6
    "three > two > one" | 3     | 6
  }

  def "writeCharacters (range) should NOT use CDATA section if the text portion contains none of '&, <, >'"(String textContent, int start, int len) {
    when:
    writer.writeCharacters(textContent.toCharArray(), start, len)

    then:
    0 * delegate.writeCData(_)
    1 * delegate.writeCharacters(textContent.substring(start, start + len))

    where:
    textContent            | start | len
    "no special character" | 4     | 10
    "one & two & three"    | 6     | 3
  }

  def "writeStartDocument should enforce writing of the [encoding] attribute"(String encoding) {
    given:
    writer = new CDataAwareXMLStreamWriter(delegate, encoding)

    when: 'writeStartDocument is called with no parameter'
    writer.writeStartDocument()

    then: 'the call should be redirected to the delegate specifying [encoding] and [version]'
    0 * delegate.writeStartDocument()
    1 * delegate.writeStartDocument(encoding, "1.0")

    when: 'writeStartDocument is called with only the version parameter'
    writer.writeStartDocument("version")

    then: 'the call should be redirected to the delegate specifying [encoding] and [version]'
    0 * delegate.writeStartDocument("version")
    1 * delegate.writeStartDocument(encoding, "version")

    where:
    encoding << [ "ISO-8859-1", "UTF-8", "cp1252" ]
  }
}
