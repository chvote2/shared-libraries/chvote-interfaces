/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream

import ch.ge.ve.interfaces.xml.XmlSource
import ch.ge.ve.interfaces.xml.codec.CodecConfiguration
import ch.ge.ve.interfaces.xml.validation.BusinessValidationException
import ch.ge.ve.interfaces.xml.validation.Violation
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRule
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRulesFactory
import java.util.stream.Collectors
import spock.lang.Specification

class Ech228StreamingCodecTest extends Specification {

  private static final String SAMPLE_FILE_PATH = "/sample-eCH-0228-v1.xml"

  def "read sample ech228"() {
    given:
    def codec = new XmlStreamingCodecFactory().ech228StreamingCodec()

    when:
    def streamedEch228 = codec.deserialize(sampleFile())

    then:
    streamedEch228.context.header.messageId == "9afd06b8-d7f7-42e2-9732-bb344f8336f8"
    streamedEch228.context.contestData.get().contestIdentification == "201901VP"
    streamedEch228.context.logisticCodes.map({ it.codeDesignation }).collect(Collectors.toList()) == ["TEST1", "TEST2"]
    streamedEch228.stream().count() == 11

    def firstVotingCard = streamedEch228.stream().findFirst().get()
    firstVotingCard.votingCardsequenceNumber == 10
    firstVotingCard.EVotingIndividualCodes.vote.ballot[0].size() == 4
    firstVotingCard.votingPerson.person.swiss.swissDomesticPerson.extension.postageCode == 2

    def lastVotingCard = streamedEch228.stream().collect(Collectors.toList()).last()
    lastVotingCard.votingCardsequenceNumber == 3
    lastVotingCard.EVotingIndividualCodes.vote.ballot[0].size() == 4
    lastVotingCard.votingPerson.person.swiss.swissDomesticPerson.extension.postageCode == 1
  }

  def "it should apply business validation rules during deserialization when validation is enabled"() {
    given:
    def rules = [Mock(StreamedValidationRule), Mock(StreamedValidationRule)]
    def rulesFactory = Mock(StreamedValidationRulesFactory)
    def codec = new XmlStreamingCodecFactory(rulesFactory).ech228StreamingCodec()

    when:
    codec.deserialize(sampleFile())

    then:
    1 * rulesFactory.createEch228ValidationRules(_) >> rules
    11 * rules[0].onElement(_)
    11 * rules[1].onElement(_)
    1 * rules[0].validate() >> []
    1 * rules[1].validate() >> []
  }

  def "it should throw a business validation exception if some rules report violations during deserialization"() {
    given:
    def rules = [Mock(StreamedValidationRule), Mock(StreamedValidationRule), Mock(StreamedValidationRule)]
    def rulesFactory = Mock(StreamedValidationRulesFactory)
    def codec = new XmlStreamingCodecFactory(rulesFactory).ech228StreamingCodec()
    rules[0].validate() >> [new Violation("rule1", "error1")]
    rules[1].validate() >> []
    rules[2].validate() >> [new Violation("rule3", "error1"), new Violation("rule3", "error2")]
    rulesFactory.createEch228ValidationRules(_) >> rules

    when:
    codec.deserialize(sampleFile())

    then:
    def exception = thrown(BusinessValidationException)
    exception.violations == [
        new Violation("rule1", "error1"), new Violation("rule3", "error1"), new Violation("rule3", "error2")
    ]
  }

  def "it should ignore disabled validation rules during deserialization"() {
    given:
    def rules = [Mock(StreamedValidationRule), Mock(StreamedValidationRule)]
    def rulesFactory = Mock(StreamedValidationRulesFactory)
    def configuration = new CodecConfiguration().withDisabledValidationRules("rule1")
    def codec = new XmlStreamingCodecFactory(rulesFactory).ech228StreamingCodec(configuration)
    rules[0].getName() >> "rule1"
    rules[0].validate() >> [new Violation("rule1", "error1")]
    rules[1].validate() >> []
    rulesFactory.createEch228ValidationRules(_) >> rules

    when:
    codec.deserialize(sampleFile())

    then:
    noExceptionThrown()
  }

  def "it should not apply business validation rules during deserialization when validation is disabled"() {
    given:
    def rulesFactory = Mock(StreamedValidationRulesFactory)
    def configuration = new CodecConfiguration().withValidationDisabled()
    def codec = new XmlStreamingCodecFactory(rulesFactory).ech228StreamingCodec(configuration)

    when:
    codec.deserialize(sampleFile())

    then:
    0 * rulesFactory.createEch228ValidationRules(_) >> []
  }

  private static XmlSource sampleFile() {
    return { getClass().getResourceAsStream(SAMPLE_FILE_PATH) }
  }
}
