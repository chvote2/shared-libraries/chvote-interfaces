/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec;

import ch.ge.ve.interfaces.xml.XmlSource;
import ch.ge.ve.interfaces.xml.validation.XmlValidationException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.function.Function;

/**
 * ChVote XML encoder/decoder. Instances of this codec are only suitable for XML object models that fit in physical
 * memory.
 *
 * @param <T> the type of objects being encoded/decoded.
 *
 * @see XmlCodecFactory
 */
public interface XmlCodec<T> {

  /**
   * Reads the given XML source and deserializes it into the corresponding Java object.
   *
   * @param source the source to read.
   *
   * @return the deserialized object.
   *
   * @throws XmlDeserializationException if the stream can't be parsed.
   * @throws XmlValidationException      if the given XML stream violates its schema or any additional "business" rule.
   */
  default T deserialize(XmlSource source) {
    return source.read((Function<InputStream, T>) this::deserialize);
  }

  /**
   * Reads the given XML stream and deserializes it into the corresponding Java object.
   *
   * @param stream the stream to read from.
   *
   * @return the deserialized object.
   *
   * @throws XmlDeserializationException if the stream can't be parsed.
   * @throws XmlValidationException      if the given XML stream violates its schema or any additional "business" rule.
   */
  T deserialize(InputStream stream);

  /**
   * Serializes the given object and writes it to the specified stream.
   *
   * @param object the object to serialize.
   * @param stream the stream to write to.
   *
   * @throws XmlSerializationException if the given object can't be serialized.
   * @throws XmlValidationException    if the produced XML stream violates its schema or any additional "business" rule.
   */
  void serialize(T object, OutputStream stream);
}
