/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.validation.rule;

import ch.ge.ve.interfaces.xml.countingcircle.v1.CountingCircleListDelivery;
import java.util.List;

/**
 * {@link ValidationRule} factory. Implementations are required to be thread safe.
 */
public interface ValidationRulesFactory {

  /**
   * Creates and returns eCH-0110 validation rules.
   *
   * @return eCH-0110 validation rules.
   */
  List<ValidationRule<ch.ge.ve.interfaces.xml.ech.eCH0110.v4.Delivery>> createEch110ValidationRules();

  /**
   * Creates and returns eCH-0157 validation rules.
   *
   * @return eCH-0157 validation rules.
   */
  List<ValidationRule<ch.ge.ve.interfaces.xml.ech.eCH0157.v4.Delivery>> createEch157ValidationRules();

  /**
   * Creates and returns eCH-0159 validation rules.
   *
   * @return eCH-0159 validation rules.
   */
  List<ValidationRule<ch.ge.ve.interfaces.xml.ech.eCH0159.v4.Delivery>> createEch159ValidationRules();

  /**
   * Creates and returns logistic file validation rules.
   *
   * @return logistic file validation rules.
   */
  List<ValidationRule<ch.ge.ve.interfaces.xml.logistic.Delivery>> createLogisticValidationRules();

  /**
   * Creates and returns counting circle list validation rules.
   *
   * @return counting circle list validation rules.
   */
  List<ValidationRule<CountingCircleListDelivery>> createCountingCircleListValidationRules();
}
