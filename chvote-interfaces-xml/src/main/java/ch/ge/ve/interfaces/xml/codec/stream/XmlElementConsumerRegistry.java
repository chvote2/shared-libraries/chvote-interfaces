/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream;

import ch.ge.ve.interfaces.xml.codec.XmlDeserializationException;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLStreamReader;

/**
 * Component where consumer functions can be registered per element. Elements are defined by their tag name and
 * unmarshalling type. The associated consumer function gets called when such an element is encountered. Not thread
 * safe.
 */
final class XmlElementConsumerRegistry {

  private final Map<String, Entry> registry;
  private final Unmarshaller       unmarshaller;

  public XmlElementConsumerRegistry(Unmarshaller unmarshaller) {
    this.registry = new ConcurrentHashMap<>();
    this.unmarshaller = unmarshaller;
  }

  public <T> void registerConsumer(String name, Class<T> targetType, Consumer<T> consumer) {
    registry.put(
        Objects.requireNonNull(name, "Element name must be specified"),
        new Entry(targetType, consumer)
    );
  }

  // Note: if the reader is not positioned on a START_ELEMENT an exception will be raised.
  public boolean consumeCurrentElement(XMLStreamReader reader) {
    String localName = reader.getLocalName();
    if (registry.containsKey(localName)) {
      unmarshalAndConsume(reader, registry.get(localName));
      return true;
    }
    return false;
  }

  @SuppressWarnings("unchecked") // safe by entry registration
  private void unmarshalAndConsume(XMLStreamReader reader, Entry entry) {
    try {
      Object value = unmarshaller.unmarshal(reader, entry.targetType).getValue();
      entry.consumer.accept(value);
    } catch (JAXBException e) {
      throw new XmlDeserializationException(e);
    }
  }

  private static class Entry {
    private final Class    targetType;
    private final Consumer consumer;

    Entry(Class targetType, Consumer consumer) {
      this.targetType = Objects.requireNonNull(targetType, "targetType must be specified");
      this.consumer = Objects.requireNonNull(consumer, "Consumer must be specified");
    }
  }
}
