/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.writer;

import ch.ge.ve.interfaces.xml.codec.XmlSerializationException;
import ch.ge.ve.interfaces.xml.ech.eCH0222.v1.Delivery;
import ch.ge.ve.interfaces.xml.util.CachingJAXBContextFactory;
import ch.ge.ve.interfaces.xml.validation.ChVoteSchema;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.channels.Channels;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;

/**
 * A factory that handles the creation of {@link Ech222Writer} instances.
 * <p>
 *   The factory hides (and caches) all {@code javax.xml} related factories and contexts
 *   and any generation configuration in order to serve document writers with minimal parameters.
 * </p>
 * <p>
 *   Usage examples :
 *   <pre>
 *     // Vote contest :
 *     Ech222WriterFactory factory = ...
 *     try (Ech222Writer writer = factory.createWriter(Paths.get("/tmp/ech-0222.xml"))) {
 *       writer.openDocument(deliveryHeader, reporting, contestIdentification)
 *             .createCountingCircle("first CountingCircle sample")
 *             .createVoteRawData("Test Vote 1")
 *             .createBallotRawData("V1 - First ballot")
 *             .appendCastBallot(ballot(1, "Q-1.1", 2))
 *             .appendCastBallot(ballot(2, "Q-1.1", 1))
 *             .createNextBallotRawData("V1 - Second ballot")
 *             .appendCastBallot(ballot(1, "Q-1.2", 1))
 *             // ...
 *             .endBallotRawDataSection()
 *             .createNextVoteRawData("Test Vote 2")
 *             // ...
 *             .endDocument()
 *     }
 *
 *     // Election contest :
 *     Ech222WriterFactory factory = ...
 *     try (Ech222Writer writer = factory.createWriter(Paths.get("/tmp/ech-0222.xml"))) {
 *       writer.openDocument(deliveryHeader, reporting, contestIdentification)
 *             .createCountingCircle("first CountingCircle sample")
 *             .createElectionGroupBallotRawData("1119")
 *             .createElectionRawData("1119")
 *             .appendCastBallot(firstBallot())
 *             .appendCastBallot(secondBallot())
 *             // ...
 *             .createNextElectionRawData()
 *             .appendCastBallot(anotherBallot())
 *             // ...
 *             .endDocument()
 *     }
 *   </pre>
 */
public class Ech222WriterFactory {

  private final JAXBContext      jaxbContext;
  private final XMLOutputFactory xmlOutputFactory;
  private final XMLEventFactory  eventFactory;

  private final boolean validation;

  /**
   * Instantiates a factory by specifying all XML-related instances to use.
   *
   * @param xmlOutputFactory the factory that will create XML writers
   * @param xmlEventFactory the factory that will create XML events
   * @param validation if XSD validation must be performed on generated eCH-0222 documents
   */
  public Ech222WriterFactory(XMLOutputFactory xmlOutputFactory, XMLEventFactory xmlEventFactory, boolean validation) {
    this.jaxbContext = CachingJAXBContextFactory.getJaxbContextFor(Delivery.class);
    this.xmlOutputFactory = xmlOutputFactory;
    this.eventFactory = xmlEventFactory;
    this.validation = validation;
  }

  /**
   * Instantiates a factory with default XML-related instances.
   *
   * @param validation if XSD validation must be performed on generated eCH-0222 documents
   *
   * @see XMLOutputFactory#newInstance()
   * @see XMLEventFactory#newInstance()
   */
  public Ech222WriterFactory(boolean validation) {
    this(XMLOutputFactory.newInstance(), XMLEventFactory.newInstance(), validation);
  }

  /**
   * Instantiates a factory with default XML-related instances and XSD validation activated.
   *
   * @see XMLOutputFactory#newInstance()
   * @see XMLEventFactory#newInstance()
   */
  public Ech222WriterFactory() {
    this(true);
  }

  /**
   * Creates a new writer that will print the generated document to the given path.
   * <p>
   *   The expected idiom is to use a {@code try-with-resource} construct, so that the underlying
   *   filesystem resource will be freed after the document generation is finished, both in case of
   *   success or failure. <br>
   *   See examples {@link Ech222WriterFactory here}.
   *
   * @param outputFile the path to where the document should be written
   * @return a new instance of Ech222Writer
   *
   * @throws IOException if an IO error occurs while opening the stream to the given path
   */
  public Ech222Writer createWriter(Path outputFile) throws IOException {
    return validation ? createWithValidation(outputFile): createNoValidation(outputFile);
  }

  // creates the writer based on a file channel to read (for validation) before the OutputStream is released
  private Ech222Writer createWithValidation(Path outputFile) throws IOException {
    final SeekableByteChannel channel = Files.newByteChannel(
        outputFile,
        StandardOpenOption.CREATE,StandardOpenOption.READ,StandardOpenOption.WRITE,StandardOpenOption.TRUNCATE_EXISTING
    );
    final Runnable validationCallback = () -> validate(channel);

    return new Ech222Writer(initEchWriter(Channels.newOutputStream(channel)), validationCallback);
  }

  private static void validate(SeekableByteChannel channel) {
    try {
      // positions the channel at the beginning of the file, then runs the schema validation
      channel.position(0L);
      ChVoteSchema.validate(Channels.newInputStream(channel));
      channel.position(channel.size());
    } catch (IOException ioe) {
      throw new UncheckedIOException("An error occurred while trying to validate the resulting file with the XSD", ioe);
    }
  }

  // creates the writer with a basic Files.newOutputStream and no validation callback
  private Ech222Writer createNoValidation(Path outputFile) throws IOException {
    final OutputStream outputStream = Files.newOutputStream(outputFile, StandardOpenOption.TRUNCATE_EXISTING);
    return new Ech222Writer(initEchWriter(outputStream));
  }

  private EchWriter initEchWriter(OutputStream outputStream) throws IOException {
    try {
      final OutputStream optimizedStream = new BufferedOutputStream(outputStream);
      // On my machine with a given file, using a BufferedOutputStream lowers the gen duration from 6mn 48s to < 40s ...
      final XMLEventWriter eventWriter = xmlOutputFactory.createXMLEventWriter(optimizedStream, StandardCharsets.UTF_8.name());
      final Marshaller marshaller = jaxbContext.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_ENCODING, StandardCharsets.UTF_8.name());
      return new EchWriter(marshaller, eventFactory, eventWriter, Ech222Writer.getAllNamespaces());
    } catch (XMLStreamException | JAXBException e) {
      outputStream.close();
      throw new XmlSerializationException("Initialization of EchWriter failed", e);
    }
  }

}
