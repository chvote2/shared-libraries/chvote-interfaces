/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec;

import java.util.regex.Pattern;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/**
 * An {@link XMLStreamWriter} that wraps text content to a CDATA section if appropriate.
 * <p>
 *   When the text content contains at least one of the characters {@code "&, <, >"}, this writer
 *   puts the whole text in a CDATA section.
 * </p>
 * <p>
 *   <strong>Implementation notes :</strong>
 *   This writer also enforces that both encoding and XML version are written to the XML declaration tag:
 *   <code>&lt;?xml encoding=".." version=".." &gt;</code>. This information is used by XML parsers to correctly
 *   resolve the charset to use when parsing documents. Though optional, it is considered safer to make it explicit.
 *   <i>This feature could / should reside in a dedicated class, but since this one is (package-) private
 *   and is our only custom implementation of {@code XMLStreamWriter}, in order to reduce classes at runtime
 *   we added the feature here... for now.</i>
 * </p>
 */
class CDataAwareXMLStreamWriter extends DelegatingXMLStreamWriter {

  private static final Pattern ESCAPED_CHARS = Pattern.compile("[&<>]");

  private final String encoding;

  /**
   * Create a writer specifying the delegate stream and the encoding.
   * <p>
   * No check is made that the given encoding matches the writer's.
   * Some implementations (eg. Sun's) of {@code XMLStreamWriter.writeStartDocument(encoding, version)}
   * do fail if the given encoding and the underlying writer's don't match, which would be sufficient.
   *
   * @param del the delegate {@code XMLStreamWriter}
   * @param encoding the encoding to be written in XML declaration
   */
  CDataAwareXMLStreamWriter(XMLStreamWriter del, String encoding) {
    super(del);
    this.encoding = encoding;
  }

  @Override
  public void writeStartDocument() throws XMLStreamException {
    super.writeStartDocument(encoding, "1.0");
  }

  @Override
  public void writeStartDocument(String version) throws XMLStreamException {
    super.writeStartDocument(encoding, version);
  }

  @Override
  public void writeCharacters(String text) throws XMLStreamException {
    if (ESCAPED_CHARS.matcher(text).find()) {
      super.writeCData(text);
    } else {
      super.writeCharacters(text);
    }
  }

  @Override
  public void writeCharacters(char[] text, int start, int len) throws XMLStreamException {
    String substring = new String(text, start, len);
    this.writeCharacters(substring);
  }
}
