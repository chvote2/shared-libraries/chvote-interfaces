/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream;

import ch.ge.ve.interfaces.xml.codec.stream.model.CastBallot;
import ch.ge.ve.interfaces.xml.ech.eCH0222.v1.VoteRawDataType.BallotRawData.BallotCasted;

/**
 * Builder for {@link CastBallot}.
 */
final class CastBallotBuilder {

  private String       countingCircleId;
  private String       contestIdentification;
  private String       voteIdentification;
  private String       ballotIdentification;
  private BallotCasted ballotCasted;

  public CastBallotBuilder countingCircleId(String countingCircleId) {
    this.countingCircleId = countingCircleId;
    return this;
  }

  public String countingCircleId() {
    return countingCircleId;
  }

  public CastBallotBuilder contestIdentification(String contestIdentification) {
    this.contestIdentification = contestIdentification;
    return this;
  }

  public String contestIdentification() {
    return contestIdentification;
  }

  public CastBallotBuilder voteIdentification(String voteIdentification) {
    this.voteIdentification = voteIdentification;
    return this;
  }

  public String voteIdentification() {
    return voteIdentification;
  }

  public CastBallotBuilder ballotIdentification(String ballotIdentification) {
    this.ballotIdentification = ballotIdentification;
    return this;
  }

  public String ballotIdentification() {
    return ballotIdentification;
  }

  public CastBallotBuilder ballotCasted(BallotCasted ballotCasted) {
    this.ballotCasted = ballotCasted;
    return this;
  }

  public BallotCasted ballotCasted() {
    return ballotCasted;
  }

  public CastBallot build() {
    return new CastBallot(countingCircleId, contestIdentification, voteIdentification,
                          ballotIdentification, ballotCasted);
  }
}