/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec;

import ch.ge.ve.interfaces.xml.util.CachingJAXBContextFactory;
import ch.ge.ve.interfaces.xml.util.CachingSAXParserFactory;
import ch.ge.ve.interfaces.xml.validation.BusinessValidationException;
import ch.ge.ve.interfaces.xml.validation.ChVoteSchema;
import ch.ge.ve.interfaces.xml.validation.SchemaValidationException;
import ch.ge.ve.interfaces.xml.validation.Violation;
import ch.ge.ve.interfaces.xml.validation.rule.ValidationRule;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 * Implementation of {@code XmlCodec} based on JAXB.
 *
 * @param <T> the type of objects being encoded/decoded.
 */
final class JAXBXmlCodecImpl<T> implements XmlCodec<T> {

  private final CodecConfiguration                configuration;
  private final JAXBContext                       context;
  private final Class<T>                          targetType;
  private final XMLOutputFactory                  xmlOutputFactory;
  private final Supplier<List<ValidationRule<T>>> validationRules;

  /**
   * Creates a new {@code JAXBXmlCodecImpl} instance.
   *
   * @param targetType      the Java class corresponding to the handled XML.
   * @param outputFactory   the output factory to use for marshalling.
   * @param configuration   codec configuration.
   * @param validationRules business validation rules associated to the handled XML.
   *
   * @throws NullPointerException if any of the given arguments is {@code null}.
   */
  JAXBXmlCodecImpl(Class<T> targetType, XMLOutputFactory outputFactory,
                   CodecConfiguration configuration, Supplier<List<ValidationRule<T>>> validationRules) {
    this.targetType = Objects.requireNonNull(targetType);
    this.context = CachingJAXBContextFactory.getJaxbContextFor(targetType);
    this.xmlOutputFactory = Objects.requireNonNull(outputFactory);
    this.configuration = Objects.requireNonNull(configuration);
    this.validationRules = Objects.requireNonNull(validationRules);
  }

  @Override
  public T deserialize(InputStream stream) {
    try {
      Unmarshaller unmarshaller = context.createUnmarshaller();
      if (configuration.isValidationEnabled()) {
        unmarshaller.setSchema(ChVoteSchema.get());
      }
      XMLReader reader = CachingSAXParserFactory.getInstance().newSAXParser().getXMLReader();
      SAXSource xml = new SAXSource(reader, new InputSource(stream));
      T object = targetType.cast(unmarshaller.unmarshal(xml));
      if (configuration.isValidationEnabled()) {
        checkBusinessRules(object);
      }
      return object;
    } catch (JAXBException e) {
      if (isSchemaValidationException(e)) {
        throw new SchemaValidationException("Source is not XSD-valid", e);
      }
      throw new XmlDeserializationException(e);
    } catch (SAXException | ParserConfigurationException e) {
      throw new XmlDeserializationException(e);
    }
  }

  @Override
  public void serialize(T object, OutputStream stream) {
    try {
      Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, configuration.isFormattedOutputEnabled());
      marshaller.setProperty(Marshaller.JAXB_ENCODING, StandardCharsets.UTF_8.name());
      if (configuration.isValidationEnabled()) {
        marshaller.setSchema(ChVoteSchema.get());
      }
      doMarshal(marshaller, object, stream);
      // validation rules must only be invoked if validation is enabled and the produced XML is XSD-valid.
      if (configuration.isValidationEnabled()) {
        checkBusinessRules(object);
      }
    } catch (JAXBException e) {
      if (isSchemaValidationException(e)) {
        throw new SchemaValidationException("Source is not XSD-valid", e);
      }
      throw new XmlDeserializationException(e);
    } catch (XMLStreamException e) {
      throw new XmlDeserializationException(e);
    }
  }

  private void doMarshal(Marshaller marshaller, Object object, OutputStream stream)
      throws JAXBException, XMLStreamException {
    if (configuration.isEscapeUsingCDataSectionEnabled()) {
      // In case of an XMLStreamWriter it should use (and output) the appropriate encoding (from marshaller - or default)
      final String encoding = Objects.toString(marshaller.getProperty(Marshaller.JAXB_ENCODING), Charset.defaultCharset().name());
      XMLStreamWriter xmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(stream, encoding);
      marshaller.marshal(object, new CDataAwareXMLStreamWriter(xmlStreamWriter, encoding));
    } else {
      marshaller.marshal(object, stream);
    }
  }

  private boolean isSchemaValidationException(JAXBException e) {
    // this is a pretty good first approximation, but it should be possible to improve this...
    return configuration.isValidationEnabled() && e.getCause() instanceof SAXException;
  }

  private void checkBusinessRules(T object) {
    List<Violation> violations =
        validationRules.get().stream()
                       .filter(rule -> !configuration.getDisabledValidationRules().contains(rule.getName()))
                       .flatMap(rule -> rule.validate(object).stream())
                       .collect(Collectors.toList());
    if (!violations.isEmpty()) {
      throw new BusinessValidationException(violations);
    }
  }
}
