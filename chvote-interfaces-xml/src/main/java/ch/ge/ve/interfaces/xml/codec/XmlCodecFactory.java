/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec;

import ch.ge.ve.interfaces.xml.countingcircle.v1.CountingCircleListDelivery;
import ch.ge.ve.interfaces.xml.ech.eCH0071.v1.Nomenclature;
import ch.ge.ve.interfaces.xml.validation.rule.ValidationRule;
import ch.ge.ve.interfaces.xml.validation.rule.ValidationRulesFactory;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;
import javax.xml.stream.XMLOutputFactory;

/**
 * {@code XmlCodec} factory for eCH-0110, eCH-0157, eCH-0159, logistic and counting circle list files. Instances of this
 * class as well as {@code XmlCodec}s they return are thread safe.
 */
public final class XmlCodecFactory {

  private static final CodecConfiguration DEFAULT_CONFIGURATION = new CodecConfiguration();

  private final ValidationRulesFactory validationRulesFactory;
  private final XMLOutputFactory       xmlOutputFactory;

  /**
   * Creates a new {@code XmlCodecFactory}.
   *
   * @param rulesFactories the "business" rules factories.
   *
   * @throws NullPointerException if {@code rulesFactories} is {@code null} of if it contains a {@code null} reference.
   */
  public XmlCodecFactory(ValidationRulesFactory... rulesFactories) {
    this(Arrays.asList(rulesFactories));
  }

  /**
   * Creates a new {@code XmlCodecFactory}.
   *
   * @param rulesFactories the "business" rules factories.
   *
   * @throws NullPointerException if {@code rulesFactories} is {@code null} of if it contains a {@code null} reference.
   */
  public XmlCodecFactory(Collection<ValidationRulesFactory> rulesFactories) {
    this.validationRulesFactory = new AggregatingValidationRulesFactory(rulesFactories);
    this.xmlOutputFactory = XMLOutputFactory.newFactory();
  }

  /**
   * Returns an {@code XmlCodec} instance with a default configuration to read/write eCH-0071 files.
   *
   * @return an {@code XmlCodec} instance to read/write eCH-0071 files.
   */
  public XmlCodec<Nomenclature> ech71Codec() {
    return ech71Codec(DEFAULT_CONFIGURATION);
  }

  /**
   * Returns an {@code XmlCodec} instance to read/write eCH-0071 files.
   *
   * @param configuration custom codec configuration.
   *
   * @return an {@code XmlCodec} instance to read/write eCH-0071 files.
   */
  public XmlCodec<Nomenclature> ech71Codec(CodecConfiguration configuration) {
    Supplier<List<ValidationRule<Nomenclature>>> validationRules = Collections::emptyList;
    return new JAXBXmlCodecImpl<>(Nomenclature.class, xmlOutputFactory, configuration, validationRules);
  }

  /**
   * Returns an {@code XmlCodec} instance with a default configuration to read/write eCH-0110 files.
   *
   * @return an {@code XmlCodec} instance to read/write eCH-0110 files.
   */
  public XmlCodec<ch.ge.ve.interfaces.xml.ech.eCH0110.v4.Delivery> ech110Codec() {
    return ech110Codec(DEFAULT_CONFIGURATION);
  }

  /**
   * Returns an {@code XmlCodec} instance to read/write eCH-0110 files.
   *
   * @param configuration custom codec configuration.
   *
   * @return an {@code XmlCodec} instance to read/write eCH-0110 files.
   *
   * @throws NullPointerException if {@code configuration} is {@code null}.
   */
  public XmlCodec<ch.ge.ve.interfaces.xml.ech.eCH0110.v4.Delivery> ech110Codec(CodecConfiguration configuration) {
    return new JAXBXmlCodecImpl<>(ch.ge.ve.interfaces.xml.ech.eCH0110.v4.Delivery.class, xmlOutputFactory,
                                  configuration, validationRulesFactory::createEch110ValidationRules);
  }

  /**
   * Returns an {@code XmlCodec} instance with a default configuration to read/write eCH-0157 files.
   *
   * @return an {@code XmlCodec} instance to read/write eCH-0157 files.
   */
  public XmlCodec<ch.ge.ve.interfaces.xml.ech.eCH0157.v4.Delivery> ech157Codec() {
    return ech157Codec(DEFAULT_CONFIGURATION);
  }

  /**
   * Returns an {@code XmlCodec} instance to read/write eCH-0157 files.
   *
   * @param configuration custom codec configuration.
   *
   * @return an {@code XmlCodec} instance to read/write eCH-0157 files.
   *
   * @throws NullPointerException if {@code configuration} is {@code null}.
   */
  public XmlCodec<ch.ge.ve.interfaces.xml.ech.eCH0157.v4.Delivery> ech157Codec(CodecConfiguration configuration) {
    return new JAXBXmlCodecImpl<>(ch.ge.ve.interfaces.xml.ech.eCH0157.v4.Delivery.class, xmlOutputFactory,
                                  configuration, validationRulesFactory::createEch157ValidationRules);
  }

  /**
   * Returns an {@code XmlCodec} instance with a default configuration to read/write eCH-0159 files.
   *
   * @return an {@code XmlCodec} instance to read/write eCH-0159 files.
   */
  public XmlCodec<ch.ge.ve.interfaces.xml.ech.eCH0159.v4.Delivery> ech159Codec() {
    return ech159Codec(DEFAULT_CONFIGURATION.withEscapeUsingCDataSectionEnabled());
  }

  /**
   * Returns an {@code XmlCodec} instance to read/write eCH-0159 files.
   *
   * @param configuration custom codec configuration.
   *
   * @return an {@code XmlCodec} instance to read/write eCH-0159 files.
   *
   * @throws NullPointerException if {@code configuration} is {@code null}.
   */
  public XmlCodec<ch.ge.ve.interfaces.xml.ech.eCH0159.v4.Delivery> ech159Codec(CodecConfiguration configuration) {
    return new JAXBXmlCodecImpl<>(ch.ge.ve.interfaces.xml.ech.eCH0159.v4.Delivery.class, xmlOutputFactory,
                                  configuration, validationRulesFactory::createEch159ValidationRules);
  }

  /**
   * Returns an {@code XmlCodec} instance with a default configuration to read/write logistic files.
   *
   * @return an {@code XmlCodec} instance to read/write logistic files.
   */
  public XmlCodec<ch.ge.ve.interfaces.xml.logistic.Delivery> logisticCodec() {
    return logisticCodec(DEFAULT_CONFIGURATION);
  }

  /**
   * Returns an {@code XmlCodec} instance to read/write logistic files.
   *
   * @param configuration custom codec configuration.
   *
   * @return an {@code XmlCodec} instance to read/write logistic files.
   *
   * @throws NullPointerException if {@code configuration} is {@code null}.
   */
  public XmlCodec<ch.ge.ve.interfaces.xml.logistic.Delivery> logisticCodec(CodecConfiguration configuration) {
    return new JAXBXmlCodecImpl<>(ch.ge.ve.interfaces.xml.logistic.Delivery.class, xmlOutputFactory,
                                  configuration, validationRulesFactory::createLogisticValidationRules);
  }

  /**
   * Returns an {@code XmlCodec} instance with a default configuration to read/write counting circle list files.
   *
   * @return an {@code XmlCodec} instance to read/write counting circle list files.
   */
  public XmlCodec<CountingCircleListDelivery> countingCircleListCodec() {
    return countingCircleListCodec(DEFAULT_CONFIGURATION);
  }

  /**
   * Returns an {@code XmlCodec} instance to read/write counting circle list files.
   *
   * @param configuration custom codec configuration.
   *
   * @return an {@code XmlCodec} instance to read/write counting circle list files.
   *
   * @throws NullPointerException if {@code configuration} is {@code null}.
   */
  public XmlCodec<CountingCircleListDelivery> countingCircleListCodec(CodecConfiguration configuration) {
    return new JAXBXmlCodecImpl<>(CountingCircleListDelivery.class, xmlOutputFactory,
                                  configuration, validationRulesFactory::createCountingCircleListValidationRules);
  }
}
