/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream;

import ch.ge.ve.interfaces.xml.XmlSource;
import ch.ge.ve.interfaces.xml.codec.XmlDeserializationException;
import ch.ge.ve.interfaces.xml.validation.XmlValidationException;

/**
 * ChVote streaming XML encoder/decoder. Instances of this codec are suitable for handling large XML streams.
 *
 * @param <S> the type of the streamed XML.
 */
public interface XmlStreamingCodec<S extends StreamedXml> {

  /**
   * Reads the given XML stream and deserializes it into the corresponding Java object.
   *
   * @param source the source to read from.
   *
   * @return the deserialized object.
   *
   * @throws XmlDeserializationException if the stream can't be parsed.
   * @throws XmlValidationException      if the given XML stream violates its schema or any additional "business" rule.
   */
  S deserialize(XmlSource source);
}
