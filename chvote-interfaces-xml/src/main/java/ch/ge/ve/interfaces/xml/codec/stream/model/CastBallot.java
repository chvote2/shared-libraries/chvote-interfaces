/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream.model;

import ch.ge.ve.interfaces.xml.ech.eCH0222.v1.VoteRawDataType.BallotRawData.BallotCasted;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Represents the element iterated over and its context when reading an eCH-0222 file.
 */
public final class CastBallot {

  private final String       countingCircleId;
  private final String       contestIdentification;
  private final String       voteIdentification;
  private final String       ballotIdentification;
  private final BallotCasted ballotCasted;

  public CastBallot(String countingCircleId, String contestIdentification, String voteIdentification,
                    String ballotIdentification, BallotCasted ballotCasted) {
    this.countingCircleId = countingCircleId;
    this.contestIdentification = contestIdentification;
    this.voteIdentification = voteIdentification;
    this.ballotIdentification = ballotIdentification;
    this.ballotCasted = ballotCasted;
  }

  public String getCountingCircleId() {
    return countingCircleId;
  }

  public String getContestIdentification() {
    return contestIdentification;
  }

  public String getVoteIdentification() {
    return voteIdentification;
  }

  public String getBallotIdentification() {
    return ballotIdentification;
  }

  public BallotCasted getBallotCasted() {
    return ballotCasted;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CastBallot that = (CastBallot) o;
    return Objects.equals(countingCircleId, that.countingCircleId) &&
           Objects.equals(contestIdentification, that.contestIdentification) &&
           Objects.equals(voteIdentification, that.voteIdentification) &&
           Objects.equals(ballotIdentification, that.ballotIdentification) &&
           Objects.equals(ballotCasted.getBallotCastedNumber(), that.ballotCasted.getBallotCastedNumber()) &&
           Objects.equals(questionsIdentification(ballotCasted), questionsIdentification(that.ballotCasted));
  }

  @Override
  public int hashCode() {
    return Objects.hash(countingCircleId, contestIdentification, voteIdentification, ballotIdentification);
  }

  private static List<String> questionsIdentification(BallotCasted ballot) {
    return ballot.getQuestionRawData().stream()
                 .map(BallotCasted.QuestionRawData::getQuestionIdentification)
                 .collect(Collectors.toList());
  }
}
