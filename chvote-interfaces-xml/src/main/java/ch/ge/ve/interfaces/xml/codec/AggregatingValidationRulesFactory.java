/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec;

import ch.ge.ve.interfaces.xml.countingcircle.v1.CountingCircleListDelivery;
import ch.ge.ve.interfaces.xml.ech.eCH0110.v4.Delivery;
import ch.ge.ve.interfaces.xml.validation.rule.ValidationRule;
import ch.ge.ve.interfaces.xml.validation.rule.ValidationRulesFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * A {@code ValidationRulesFactory} implementation that aggregates results from multiple instances.
 */
final class AggregatingValidationRulesFactory implements ValidationRulesFactory {

  private final List<ValidationRulesFactory> factories;

  /**
   * Creates a new {@code AggregatingValidationRulesFactory}.
   *
   * @param factories the factories to aggregate.
   *
   * @throws NullPointerException if {@code factories} is {@code null} or if it contains a {@code null} reference.
   */
  AggregatingValidationRulesFactory(Collection<ValidationRulesFactory> factories) {
    this.factories = Collections.unmodifiableList(new ArrayList<>(factories));
    this.factories.forEach(Objects::requireNonNull);
  }

  @Override
  public List<ValidationRule<Delivery>> createEch110ValidationRules() {
    return aggregateRules(ValidationRulesFactory::createEch110ValidationRules);
  }

  @Override
  public List<ValidationRule<ch.ge.ve.interfaces.xml.ech.eCH0157.v4.Delivery>> createEch157ValidationRules() {
    return aggregateRules(ValidationRulesFactory::createEch157ValidationRules);
  }

  @Override
  public List<ValidationRule<ch.ge.ve.interfaces.xml.ech.eCH0159.v4.Delivery>> createEch159ValidationRules() {
    return aggregateRules(ValidationRulesFactory::createEch159ValidationRules);
  }

  @Override
  public List<ValidationRule<ch.ge.ve.interfaces.xml.logistic.Delivery>> createLogisticValidationRules() {
    return aggregateRules(ValidationRulesFactory::createLogisticValidationRules);
  }

  @Override
  public List<ValidationRule<CountingCircleListDelivery>> createCountingCircleListValidationRules() {
    return aggregateRules(ValidationRulesFactory::createCountingCircleListValidationRules);
  }

  private <E> List<ValidationRule<E>> aggregateRules(Function<ValidationRulesFactory, List<ValidationRule<E>>> extractor) {
    return factories.stream()
                    .flatMap(factory -> extractor.apply(factory).stream())
                    .collect(Collectors.toList());
  }
}
