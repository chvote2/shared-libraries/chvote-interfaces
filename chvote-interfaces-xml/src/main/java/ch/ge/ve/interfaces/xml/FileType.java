/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml;

import ch.ge.ve.interfaces.xml.util.CachingXMLInputFactory;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.URI;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * ChVote supported XML file types.
 */
public enum FileType {

  ECH_0071("http://www.ech.ch/xmlns/eCH-0071/1"),

  ECH_0045("http://www.ech.ch/xmlns/eCH-0045/4"),

  ECH_0110("http://www.ech.ch/xmlns/eCH-0110/4"),

  ECH_0157("http://www.ech.ch/xmlns/eCH-0157/4"),

  ECH_0159("http://www.ech.ch/xmlns/eCH-0159/4"),

  ECH_0222("http://www.ech.ch/xmlns/eCH-0222/1"),

  ECH_0228("http://www.ech.ch/xmlns/eCH-0228/1"),

  LOGISTIC("http://www.ech.ch/xmlns/logistic/1"),

  COUNTING_CIRCLE_LIST("http://www.ge.ch/sidp/types/chvote/counting-circle/1.0.0");

  /**
   * Determines the type of the given XML source.
   *
   * @param source the source.
   *
   * @return the type of the given XML source.
   *
   * @throws NullPointerException     if {@code source} is {@code null}.
   * @throws IllegalArgumentException if {@code source} is not one of the supported file types.
   * @throws UncheckedIOException     if {@code source} can't be read.
   */
  public static FileType of(XmlSource source) {
    return source.read((Function<InputStream, FileType>) FileType::of);
  }

  /**
   * Determines the type of the given XML stream.
   *
   * @param stream the stream.
   *
   * @return the type of the given XML stream.
   *
   * @throws NullPointerException     if {@code source} is {@code null}.
   * @throws IllegalArgumentException if {@code source} is not one of the supported file types.
   * @throws UncheckedIOException     if {@code source} can't be read.
   */
  public static FileType of(InputStream stream) {
    Objects.requireNonNull(stream);
    XMLInputFactory xmlInputFactory = CachingXMLInputFactory.getInstance();
    try {
      XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(stream);
      while (reader.hasNext() && !reader.isStartElement()) {
        reader.next();
      }
      URI namespace = URI.create(reader.getName().getNamespaceURI());
      return Stream.of(values())
                   .filter(f -> f.namespace.equals(namespace))
                   .findFirst().orElseThrow(() -> new IllegalArgumentException("Unknown file type: " + namespace));
    } catch (XMLStreamException e) {
      throw new IllegalArgumentException("Processing error.", e);
    }
  }

  private final URI namespace;

  FileType(String namespace) {
    this.namespace = URI.create(namespace);
  }
}
