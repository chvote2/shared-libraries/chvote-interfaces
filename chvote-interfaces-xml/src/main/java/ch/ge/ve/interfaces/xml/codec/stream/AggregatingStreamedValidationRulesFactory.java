/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream;

import ch.ge.ve.interfaces.xml.codec.stream.model.CastBallot;
import ch.ge.ve.interfaces.xml.codec.stream.model.Ech222Context;
import ch.ge.ve.interfaces.xml.codec.stream.model.Ech228Context;
import ch.ge.ve.interfaces.xml.codec.stream.model.Ech45Context;
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.VotingPersonType;
import ch.ge.ve.interfaces.xml.ech.eCH0228.v1.VotingCardDeliveryType;
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRule;
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRulesFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * A {@code StreamedValidationRulesFactory} implementation that aggregates results from multiple instances.
 */
final class AggregatingStreamedValidationRulesFactory implements StreamedValidationRulesFactory {

  private final List<StreamedValidationRulesFactory> factories;

  /**
   * Creates a new {@code AggregatingStreamedValidationRulesFactory}.
   *
   * @param factories the factories to aggregate.
   *
   * @throws NullPointerException if {@code factories} is {@code null} or if it contains a {@code null} reference.
   */
  public AggregatingStreamedValidationRulesFactory(Collection<StreamedValidationRulesFactory> factories) {
    this.factories = Collections.unmodifiableList(new ArrayList<>(factories));
    this.factories.forEach(Objects::requireNonNull);
  }

  @Override
  public List<StreamedValidationRule<VotingPersonType>> createEch45ValidationRules(Ech45Context context) {
    return factories.stream().flatMap(f -> f.createEch45ValidationRules(context).stream()).collect(Collectors.toList());
  }

  @Override
  public List<StreamedValidationRule<CastBallot>> createEch222ValidationRules(Ech222Context context) {
    return factories.stream().flatMap(f -> f.createEch222ValidationRules(context).stream()).collect(Collectors.toList());
  }

  @Override
  public List<StreamedValidationRule<VotingCardDeliveryType.VotingCard>> createEch228ValidationRules(Ech228Context context) {
    return factories.stream().flatMap(f -> f.createEch228ValidationRules(context).stream()).collect(Collectors.toList());
  }
}
