/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream;

import java.io.Closeable;
import java.io.IOException;
import java.io.UncheckedIOException;

/**
 * I/O utils.
 */
final class Io {

  /**
   * Closes the given stream.
   *
   * @param stream the stream to close.
   *
   * @throws UncheckedIOException if an I/O error occurs during the process.
   */
  static void close(Closeable stream) {
    try {
      stream.close();
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  private Io() {
    throw new AssertionError("Not meant to be instantiated");
  }
}
