/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.validation.rule.common

import ch.ge.ve.interfaces.xml.codec.stream.model.Ech222Context
import ch.ge.ve.interfaces.xml.codec.stream.model.Ech228Context
import ch.ge.ve.interfaces.xml.codec.stream.model.Ech45Context
import java.util.stream.Collectors
import spock.lang.Specification

class CommonStreamedValidationRulesFactoryTest extends Specification {

  def factory = new CommonStreamedValidationRulesFactory()

  def "it should return all defined eCH-0045 validation rules"() {
    given:
    def context = new Ech45Context(null, null, 0, null)

    expect:
    factory.createEch45ValidationRules(context).stream().map({ it.class }).collect(Collectors.toSet()) == [
        Ech45NumberOfVotersValidationRule, Ech45ExtensionValidationRule, Ech45UniqueLocalPersonIdValidationRule
    ] as Set
  }

  def "it should return all defined eCH-0222 validation rules"() {
    given:
    def context = new Ech222Context(null, null, null, null)

    expect:
    factory.createEch222ValidationRules(context).stream().map({ it.class }).collect(Collectors.toSet()) == [] as Set
  }

  def "it should return all defined eCH-0228 validation rules"() {
    given:
    def context = new Ech228Context(null, null, null)

    expect:
    factory.createEch228ValidationRules(context).stream().map({ it.class }).collect(Collectors.toSet()) == [] as Set
  }
}
