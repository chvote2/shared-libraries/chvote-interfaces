/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.validation.rule.common;

import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.VotingPersonType;
import ch.ge.ve.interfaces.xml.validation.Violation;
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRule;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * {@code StreamedValidationRule} implementation that checks that declared number of voters matches the actual number
 * of voters in the stream.
 */
public final class Ech45NumberOfVotersValidationRule implements StreamedValidationRule<VotingPersonType> {

  private final long       declaredNumberOfVoters;
  private final AtomicLong votersCounter;

  Ech45NumberOfVotersValidationRule(long declaredNumberOfVoters) {
    this.declaredNumberOfVoters = declaredNumberOfVoters;
    this.votersCounter = new AtomicLong();
  }

  @Override
  public void onElement(VotingPersonType element) {
    votersCounter.incrementAndGet();
  }

  @Override
  public List<Violation> validate() {
    if (declaredNumberOfVoters == votersCounter.get()) {
      return Collections.emptyList();
    }
    String description = String.format("Declared number of voters (%s) <> actual number of voters (%s)",
                                       declaredNumberOfVoters, votersCounter.get());
    return Collections.singletonList(new Violation(getName(), description));
  }
}
